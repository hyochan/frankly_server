var Sequelize = require('sequelize');
var sequelize = new Sequelize('frankly', 'root');

var User = sequelize.define('User', {
    device_id: {type: Sequelize.STRING, unique: true },
    email: Sequelize.STRING,
    phone: Sequelize.INTEGER,
    lat: {type: Sequelize.DOUBLE,  defaultValue: 37.564174},
    lng: {type: Sequelize.DOUBLE,  defaultValue: 126.997576}
});

var Comment = sequelize.define('Comment', {
    parrent_id: {type: Sequelize.INTEGER },
    comment: {type: Sequelize.STRING },
    lat: {type: Sequelize.DOUBLE,  defaultValue: 37.564174},
    lng: {type: Sequelize.DOUBLE,  defaultValue: 126.997576}
});

module.exports.getUser = function(device_id, callback){
    User.findOne({ where: {device_id: device_id} }).then(callback);
}

module.exports.setUser = function(device_id, email, phone, lat, lng, callback){
    sequelize.sync().then(function() {
        return User.create({
                        device_id: device_id,
                        email: email,
                        phone: phone,
                        lat: lat,
                        lng: lng,
                        }).then(callback);
      });
}


module.exports.getComments = function(lat, lng, callback){
    var distance = 0.000
    Comment.findAll({ where: {
                        $and {
                            lat: {
                                $between: [lng - distance, 15]
                                },
                            lng: {
                                $between: [11, 15]
                                }
                            }
                        }
                    }).then(callback);
}