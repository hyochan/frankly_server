var db = require('../sql/tables.js');

exports.sign = function(req, res) {
    db.getUser(req.params.id, function(result){
        if (result != null) {
            res.send(result.get({plain: true}));
        } else {
            db.setUser(req.params.id,
                       req.body.email,
                       req.body.phone,
                       req.body.lat,
                       req.body.lng,
                       function(result){
                            db.getUser(req.params.id, function(result){
                                if (result != null) {
                                    res.send(result.get({plain: true}));
                                } else {
                                    res.send('null');
                                }
                            });
                       }
            );
        }
    });
};

